import 'package:flutter/material.dart';
import 'package:my_app/pages/home_page.dart';
import 'package:my_app/pages/details.dart';
import 'package:my_app/pages/gallery.dart';
import 'package:my_app/menu/about.dart';
import 'package:my_app/menu/extension.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MyHomePage();
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var theme = Brightness.light;
  var indx = 0;
  var screens = [
    const HomePage(),
    const Details(),
    const Gallery(),
    About(),
    const Demo()
  ];
  Widget scr = const HomePage();

  void changeTheme() {
    setState(() {
      if (theme == Brightness.light) {
        theme = Brightness.dark;
      } else {
        theme = Brightness.light;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'My App',
        theme: ThemeData(primarySwatch: Colors.deepPurple, brightness: theme),
        home: SafeArea(
            top: false,
            child: Container(
              color: Colors.white,
              child: Scaffold(
                key: scaffoldKey,
                appBar: AppBar(title: const Text("Crystal Ball")),
                body: scr,
                floatingActionButton: FloatingActionButton(
                    hoverColor: Colors.deepPurpleAccent,
                    onPressed: () {
                      changeTheme();
                    },
                    tooltip: 'Toggle Theme',
                    child: const Icon(Icons.dark_mode_outlined)),
                drawerEnableOpenDragGesture: true,
                drawerEdgeDragWidth: 13,
                drawer: Drawer(
                    child: ListView(
                  children: <Widget>[
                    const DrawerHeader(
                      decoration: BoxDecoration(color: Colors.deepPurpleAccent),
                      child: Text('Menu',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 23,
                              fontWeight: FontWeight.bold)),
                    ),
                    ListTile(
                        title: const Text('Quotes'),
                        leading: const Icon(Icons.quora),
                        onTap: () {
                          setState(() {
                            scr = screens[3];
                          });
                          if (scaffoldKey.currentState!.isDrawerOpen) {
                            scaffoldKey.currentState!.closeDrawer();
                            //close drawer, if drawer is open
                          } else {
                            scaffoldKey.currentState!.openDrawer();
                            //open drawer, if drawer is closed
                          }
                        }),
                    ListTile(
                        title: const Text('Extension'),
                        leading: const Icon(Icons.extension),
                        onTap: (() {
                          setState(() {
                            scr = screens[4];
                          });
                          if (scaffoldKey.currentState!.isDrawerOpen) {
                            scaffoldKey.currentState!.closeDrawer();
                            //close drawer, if drawer is open
                          } else {
                            scaffoldKey.currentState!.openDrawer();
                            //open drawer, if drawer is closed
                          }
                        })),
                  ],
                )),
                bottomNavigationBar: BottomNavigationBar(
                    currentIndex: indx,
                    backgroundColor: Colors.deepPurple,
                    fixedColor: Colors.white,
                    showUnselectedLabels: false,
                    items: const [
                      BottomNavigationBarItem(
                        label: "Home",
                        icon: Icon(Icons.home_max_outlined, size: 30),
                      ),
                      BottomNavigationBarItem(
                        label: "Details",
                        icon: Icon(Icons.details_outlined, size: 30),
                      ),
                      BottomNavigationBarItem(
                        label: "Gallery",
                        icon: Icon(Icons.browse_gallery_outlined, size: 30),
                      ),
                    ],
                    onTap: (int indexOfItem) {
                      setState(() {
                        indx = indexOfItem;
                        scr = screens[indx];
                      });
                    }),
              ),
            )));
  }
}
