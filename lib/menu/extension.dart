import 'package:flutter/material.dart';

class Demo extends StatelessWidget {
  const Demo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: const EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                Flexible(
                    child: Text("Defence Equipments",
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ))),
              ])),
          Flexible(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                Flexible(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                      Flexible(child: Icon(Icons.rocket, color: Colors.blue)),
                      Flexible(child: Text('Rocket\nTime: 8s')),
                    ])),
                Flexible(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                      Flexible(
                          child:
                              Icon(Icons.bike_scooter, color: Colors.yellow)),
                      Flexible(child: Text('Bike-Scooter\nTime: 5m')),
                    ])),
                Flexible(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                      Flexible(
                          child: Icon(Icons.fire_truck, color: Colors.red)),
                      Flexible(child: Text('Fire Truck\nTime: 18m')),
                    ]))
              ])),
        ],
      ),
    ));
  }
}
