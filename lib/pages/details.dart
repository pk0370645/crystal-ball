import 'package:flutter/material.dart';

class Details extends StatelessWidget {
  const Details({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text("Crystal Ball",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold))
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Flexible(
                      child: Text(
                          "A crystal ball, also known as an orbuculum or crystal sphere, is a crystal or glass ball and common fortune-telling object.\nIt is generally associated with the performance of clairvoyance and scrying in particular.\nIn more recent times, the crystal ball has been used for creative photography with the term lensball commonly used to describe a crystal ball used as a photography prop.",
                          style: TextStyle(fontSize: 18)),
                    )
                  ],
                ),
              ],
            )));
  }
}
