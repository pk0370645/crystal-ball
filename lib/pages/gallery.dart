import 'dart:math';
import 'package:flutter/material.dart';

class Gallery extends StatefulWidget {
  const Gallery({super.key});
  @override
  State<Gallery> createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  int num = 1;

  void incrementCounter() {
    setState(() {
      num = Random().nextInt(9);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.all(10),
        child: GestureDetector(
            onTap: () {
              incrementCounter();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin: const EdgeInsets.only(bottom: 60),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text("Food Menu",
                            style: TextStyle(
                              fontSize: 33,
                              fontWeight: FontWeight.bold,
                            ))
                      ],
                    )),
                Expanded(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                      Flexible(
                          child: Image.asset(
                        'images/food$num.jpg',
                      ))
                    ])),
              ],
            )),
      ),
    );
  }
}
